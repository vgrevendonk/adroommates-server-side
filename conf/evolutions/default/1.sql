# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table debt (
  id                        integer auto_increment not null,
  value                     double,
  from_id                   integer,
  to_id                     integer,
  related_transaction_id    integer,
  constraint pk_debt primary key (id))
;

create table member (
  id                        integer auto_increment not null,
  email_address             varchar(255),
  password                  varchar(255),
  first_name                varchar(255),
  last_name                 varchar(255),
  phone_number              varchar(255),
  balance                   double,
  constraint pk_member primary key (id))
;

create table message (
  id                        integer auto_increment not null,
  body                      varchar(255),
  added_by_id               integer,
  sent                      datetime,
  constraint pk_message primary key (id))
;

create table session (
  id                        integer auto_increment not null,
  owner_id                  integer,
  key_string                varchar(255),
  creation_date             datetime,
  constraint pk_session primary key (id))
;

create table shopping_item (
  id                        integer auto_increment not null,
  content                   varchar(255),
  added                     datetime,
  added_by_id               integer,
  constraint pk_shopping_item primary key (id))
;

create table transaction (
  id                        integer auto_increment not null,
  total_value               double,
  issuer_id                 integer,
  issued                    datetime,
  constraint pk_transaction primary key (id))
;

alter table debt add constraint fk_debt_from_1 foreign key (from_id) references member (id) on delete restrict on update restrict;
create index ix_debt_from_1 on debt (from_id);
alter table debt add constraint fk_debt_to_2 foreign key (to_id) references member (id) on delete restrict on update restrict;
create index ix_debt_to_2 on debt (to_id);
alter table debt add constraint fk_debt_relatedTransaction_3 foreign key (related_transaction_id) references transaction (id) on delete restrict on update restrict;
create index ix_debt_relatedTransaction_3 on debt (related_transaction_id);
alter table message add constraint fk_message_addedBy_4 foreign key (added_by_id) references member (id) on delete restrict on update restrict;
create index ix_message_addedBy_4 on message (added_by_id);
alter table session add constraint fk_session_owner_5 foreign key (owner_id) references member (id) on delete restrict on update restrict;
create index ix_session_owner_5 on session (owner_id);
alter table shopping_item add constraint fk_shopping_item_addedBy_6 foreign key (added_by_id) references member (id) on delete restrict on update restrict;
create index ix_shopping_item_addedBy_6 on shopping_item (added_by_id);
alter table transaction add constraint fk_transaction_issuer_7 foreign key (issuer_id) references member (id) on delete restrict on update restrict;
create index ix_transaction_issuer_7 on transaction (issuer_id);



# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table debt;

drop table member;

drop table message;

drop table session;

drop table shopping_item;

drop table transaction;

SET FOREIGN_KEY_CHECKS=1;

