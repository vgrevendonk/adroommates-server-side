package controllers;

import java.util.Calendar;
import java.util.List;

import models.Member;
import models.Session;
import play.mvc.Controller;
import play.mvc.Result;

public class Login extends Controller{
	
	public static Result showLogin(String login) {
		Result htmlResult = ok("<h1>"+ login +"</h1>").as("text/html");
		return htmlResult;
	}

	public static Result showPass(String pass) {
		Result htmlResult = ok("<h1>"+ pass +"</h1>").as("text/html");
		return htmlResult;
	}
	
	
	public static Result sessKey() {
		
		Member member = Member.findByLogin("login");
		Result htmlResult = ok("<h1>"+ member +"</h1>").as("text/html");
		return htmlResult;
	}
	
	/**
	 * Displays a session key to the user if his ids are correct, and else an
	 * error code.
	 * @return
	 */
	public static Result login(String login, String pwd) {
		//Member trying to login.
		//Check his ids.
		List<Member> l = Member.find.where().eq("email_address", login).eq("password", pwd).findList();
		
		switch(l.size()) {
		case 0:
			//Login failed, user not known.
			return forbidden(ReturnCodes.prefix(ReturnCodes.ERROR_WRONG_DATA, "Authentication failed."));
		case 1:
			//Login success.
			//New login, so first delete all sessions.
			Member member = l.get(0);
			
			List<Session> sessions = Session.find.where().eq("owner_id", Integer.valueOf(member.id)).findList();
			
			for(Session s: sessions) 
				s.delete();
			
			//Then create new session.
			Session s = new Session();
			s.owner = member;
			s.generateKey(member);
			s.creationDate = Calendar.getInstance().getTime();
			
			s.save();
			
			//And return ok
			return ok(ReturnCodes.prefix(ReturnCodes.REQUEST_ACCEPTED, s.keyString));
		default:
			//This should never happen.
			return internalServerError(ReturnCodes.prefix(ReturnCodes.ERROR_INTERNAL, "Same logins found twice."));
		}
	}
}
