package controllers;

public class ReturnCodes {
	//OK
	public static final int REQUEST_ACCEPTED = 200;
	
	//Errors
	public static final int ERROR_WRONG_DATA = 403;
	public static final int ERROR_INTERNAL = 500;
	
	//Utilities
	public static String prefix(int code, String suffix) {
		return code + ":" + suffix;
	}
}
