package controllers;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import models.Member;
import models.Session;
import models.ShoppingItem;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.node.ObjectNode;
import play.api.libs.Files;
import play.data.DynamicForm;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;


public class ShoppingList extends Controller {

	 //1.les éléments de la shopping list,
	 //2.args GET : clé session
	static File file =  new File("mutexfile");
	static int busy = 0;
	public static Result getShoppingList(String key){
		
		if(!Authentication.sessionAuthOK(key)){
			return forbidden(ReturnCodes.prefix(ReturnCodes.ERROR_WRONG_DATA, "Authentication failed!"));
		}
		
		ObjectNode result = Json.newObject();
		ObjectNode shoppingListNode = Json.newObject();
		List<ShoppingItem> items = ShoppingItem.find.all();
		
		for(ShoppingItem i: items) {
			shoppingListNode.put(Integer.toString(i.id), i.toJSON());
		}
		
		result.put("shopping_list", shoppingListNode);
		result.put("return_code",ReturnCodes.REQUEST_ACCEPTED);
		
		return ok(result).as("application/json");
		
	}
	
	/**
	 * Request mutex lock and lock it. Synchronized.
	 * @param key
	 * @return
	 */
	public static synchronized Result mutexLock (String key) {
		
		if(!Authentication.sessionAuthOK(key)){
			return forbidden(ReturnCodes.prefix(ReturnCodes.ERROR_WRONG_DATA, "Authentication failed!"));
		}
		
		//synchronized(this){SC}
		// isFichier
		// Si oui, lire valeur, (Fichier contenant id editeur)
		// Repondre occupé 
		// SI non, creation d'un fichier contenant l'ide courant
		// et repondre ok .
		
		Session s = null;
		s = Session.find.where().eq("keyString", key).findUnique();
		
		if(file.exists()){
			System.out.println("Mutex exists, rejected.");
			busy = Integer.parseInt(Files.readFile(file));
			System.out.println(Member.find.byId(busy).firstName + " was editing.");
			return ok(ReturnCodes.ERROR_WRONG_DATA + ":" + Member.find.byId(busy).firstName);
		}
		else {
			System.out.println("Mutex request accepted.");
			Files.writeFile(file, Integer.toString(s.owner.id));
			return ok(ReturnCodes.REQUEST_ACCEPTED + ":ok");

		}
		
	}
	
	/**
	 * Request mutex release and post shopping list.
	 * @param key
	 * @return
	 */
	public static Result postList(){
		DynamicForm form = form().bindFromRequest();
		String key = form.get("key");
		String tData = form.get("list");
		
		System.out.println("Transaction data: " + tData);
		
		if(!Authentication.sessionAuthOK(key)){
			System.out.println("Auth");
			return forbidden(ReturnCodes.prefix(ReturnCodes.ERROR_WRONG_DATA, "Authentication failed!"));
		}
		
		//isFichier si non, repondre ok; 
		//si oui compare(id_curr,id_fichier) si égale
		// repondre ok, suppr fichier
		// si pas égale repondre badRequest
		Session s = null;
		s = Session.find.where().eq("keyString", key).findUnique();
		
		if(file.exists() && Integer.parseInt(Files.readFile(file)) != s.owner.id){
			System.out.println("File exists but wrong usr");
			return ok(ReturnCodes.ERROR_WRONG_DATA + ":Unauthorized poster");
		}
		else if(!file.exists()) {
			System.out.println("File does not exist");
			return ok(ReturnCodes.ERROR_WRONG_DATA + ":Mutex not locked, lock first.");
		}
		
		//Parse posted list
		
		JsonNode shoppingList = Json.parse(tData);
		Iterator<JsonNode> it = shoppingList.getElements();
		
		ArrayList<ShoppingItem> postedList = new ArrayList<ShoppingItem>();
		
		while(it.hasNext()) {
			postedList.add(ShoppingItem.parseJSON(it.next()));
		}
		
		System.out.println("Got " + postedList.size() + " items by POST");
		
		//Get shopping items list
		
		List<ShoppingItem> originalList = ShoppingItem.find.all();
		
		//Compare
		
		for(ShoppingItem i: postedList) {
			if(i.id == -1) {
				i.id = 0;
				i.added = Calendar.getInstance().getTime();
				i.addedBy = s.owner;
				i.save();
			}
		}
		
		for(ShoppingItem i: originalList) {
			if(!postedList.contains(i))
				i.delete();
		}
		
		file.delete();
		
		return ok(ReturnCodes.REQUEST_ACCEPTED + ":Post successful");
	}
}
