package controllers;

import play.*;
import play.mvc.*;

import views.html.*;

@SuppressWarnings("unused")
public class Application extends Controller {
  
  public static Result index() {
    return ok(index.render("Your new application is ready."));
  }
  
  public static Result pageNotFound(String path) {
	  System.out.println("404:Page not found: " + path);
	  return badRequest("404:Page not found " + path);
  }
  
}