package controllers;

import models.Debt;
import models.Member;
import models.Session;
import models.Transaction;
import play.mvc.Controller;
import play.mvc.Result;

public class AlgoDebts extends Controller{
	
	/**
	 * Update member balances with the newest transaction
	 * @param t The latest transaction
	 */
	public static void updateBalanceWithTransaction (Transaction t, Member to) {
		for(Debt d: t.associatedDebts) {
			d.to.balance += d.value;
			d.from.balance -= d.value;
			
			System.out.println("I'm about to save inside AG");
			d.from.save();
		
			System.out.println("Saved AG");
		}
		System.out.println("before save to");
	
		to.balance += t.totalValue;
		to.save();
	
		System.out.println("after save to");
	}

}
