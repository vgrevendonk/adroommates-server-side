package controllers;

import java.util.Calendar;
import java.util.List;

import models.Message;
import models.Session;

import org.codehaus.jackson.node.ObjectNode;

import play.data.DynamicForm;
import play.libs.Json;
import play.mvc.Result;
import play.mvc.Controller;

public class Inbox extends Controller {
	public static final int MSG_REQ = 20;
	
	public static Result getMessages(String key){
		System.out.println("Get messages request");
		
		if(!Authentication.sessionAuthOK(key)){
			return forbidden(ReturnCodes.prefix(ReturnCodes.ERROR_WRONG_DATA, "Authentication failed!"));
		}
		ObjectNode result = Json.newObject();
		ObjectNode messagesNode = Json.newObject();
		List<Message> messages = Message.find.orderBy("sent desc").findList();
		for(int i = 0; i < Math.min(MSG_REQ, messages.size()); i++){
			Message m = messages.get(i);
			messagesNode.put(Integer.toString(m.id), m.toJSON());			
		}
		
		result.put("messages", messagesNode);
		result.put("return_code", ReturnCodes.REQUEST_ACCEPTED);
		
		return ok(result).as("application/json");
	}
	
	public static Result getMessagesIndex(String key, Integer id_start, Integer id_end){
		if(!Authentication.sessionAuthOK(key)){
			return forbidden(ReturnCodes.prefix(ReturnCodes.ERROR_WRONG_DATA, "Authentication failed!"));
		}
		ObjectNode result = Json.newObject();
		ObjectNode messagesNode = Json.newObject();
		List<Message> messages = Message.find.orderBy("sent desc").findList();
		if(id_start < id_end){
			Integer tmp = 0;
			id_start = tmp;
			id_start = id_end;
			id_end = tmp;
		}
		for(int i = id_start; i < Math.min(id_end, messages.size()); i++){
			Message m = messages.get(i);
			messagesNode.put(Integer.toString(m.id), m.toJSON());			
		}
		
		result.put("messages", messagesNode);
		result.put("return_code", ReturnCodes.REQUEST_ACCEPTED);
		
		return ok(result).as("application/json");
	}
	
	
	public static Result postMessages(){
		System.out.println("Got post request, testing auth");
		
		//Get arguments
		DynamicForm form = form().bindFromRequest();
		String key = form.get("key");
		String message = form.get("message");
		
		if(!Authentication.sessionAuthOK(key)){
			return forbidden(ReturnCodes.prefix(ReturnCodes.ERROR_WRONG_DATA, "Authentication failed!"));
		}
		System.out.println("Got post request, auth ok");
		
		Message m = new Message();
		Session s = Session.find.where().eq("keyString", key).findUnique();
		m.body = message;
		m.addedBy = s.owner;
		m.sent = Calendar.getInstance().getTime();
		m.save();
		
		return ok(ReturnCodes.REQUEST_ACCEPTED + ":Message sent");
		
	}
	
	
	public static Result postTest() {
		System.out.println("Received post-test");
		
		DynamicForm form = form().bindFromRequest();
		System.out.println(form.get("message"));
		
		return ok("200: Test success");
	}
}
