package controllers;

import java.util.List;

import models.Session;

public class Authentication {
	private static Session currentSession;
	
	public static boolean sessionAuthOK(String key) {
		//TODO check dates
		
		List<Session> sessions = Session.find.where().eq("key_string", key).findList();
		
		switch(sessions.size()) {
		case 0:
			return false;
		case 1:
			currentSession = sessions.get(0);
			return true;
		default:
			//Error
			return false;
		}
	}

	public static Session getCurrentSession() {
		return currentSession;
	}
}
