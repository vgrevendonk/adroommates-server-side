package controllers;

import java.util.List;

import models.Member;
import models.Session;
import models.ShoppingItem;
import models.Transaction;

import org.codehaus.jackson.node.ObjectNode;

import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;


public class GlobalInfo extends Controller {
	public static final int GI_REQUESTED_TRANSACTIONS = 5;
	
	public static Result sayHello(Session s) {
		ObjectNode result = Json.newObject();
		result.put("status", "OK");
		return ok(result).as("application/json");
	}
	
	public static Result getGlobalInfo(String key){
		
		
		if(!Authentication.sessionAuthOK(key)){
			return forbidden(ReturnCodes.prefix(ReturnCodes.ERROR_WRONG_DATA, "Authentication failed!"));
		}
		Session s = null;
		s = Session.find.where().eq("keyString", key).findUnique();
		
		//TODO check whether this session really exists and has not expired
		
		
		ObjectNode result = Json.newObject();
		
		//Global info has to contain four things: 
		// -> Last transactions
		// -> Members list
		// -> Connected member's ID
		// -> Shopping list items.
		
		//1. Connected member's ID
		
		result.put("connected_ID", s.owner.id);
		
		//2. Member's list
		
		ObjectNode membersNode = Json.newObject();
		List<Member> members = Member.find.all();
		
		for(Member m: members) {
			membersNode.put(Integer.toString(m.id), m.toJSON());
		}
		
		result.put("members", membersNode);
		
		//3. Last transactions
		
		ObjectNode transactionsNode = Json.newObject();
		List<Transaction> transactions = Transaction.find.orderBy("issued desc").findList();
		
		for(int i = 0; i < Math.min(GI_REQUESTED_TRANSACTIONS, transactions.size()); i++) {
			Transaction t = transactions.get(i);
			transactionsNode.put(Integer.toString(t.id), t.toJSON());
		}
		
		result.put("transactions", transactionsNode);
		
		//4. Shopping list
		
		ObjectNode shoppingListNode = Json.newObject();
		List<ShoppingItem> items = ShoppingItem.find.all();
		
		for(ShoppingItem i: items) {
			shoppingListNode.put(Integer.toString(i.id), i.toJSON());
		}
		
		result.put("shopping_list", shoppingListNode);
		
		result.put("return_code", ReturnCodes.REQUEST_ACCEPTED);
		
		return ok(result).as("application/json");
	}
	
	public static Result getMemberInfo(String key){
		
		
		
		return TODO;
		
	}

	

	
	
}
