package controllers;

import java.util.List;

import models.Debt;
import models.Session;
import models.Transaction;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.node.ObjectNode;

import play.data.DynamicForm;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

public class Transactions extends Controller {
	
	
	/**
	 * Transactions from start to end <em>included</em>.
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
	public static Result getTransactions(String key){
		
		if(!Authentication.sessionAuthOK(key)){
			return forbidden(ReturnCodes.prefix(ReturnCodes.ERROR_WRONG_DATA, "Authentication failed!"));
		}
		ObjectNode result = Json.newObject();
		
		ObjectNode transactionsNode = Json.newObject();
		List<Transaction> transactions = Transaction.find.orderBy("issued desc").findList();
		
		for(int i = 0; i <transactions.size(); i++) {
			Transaction t = transactions.get(i);
			transactionsNode.put(Integer.toString(t.id), t.toJSON());
		}
		
		result.put("transactions", transactionsNode);
		result.put("return_code", ReturnCodes.REQUEST_ACCEPTED);
		
		return ok(result).as("application/json");
	}
	

	/**
	 * Only one transaction is sent in post data. No need for a JSON iterator, thus.
	 * @return 
	 */
	public static Result postTransaction (){
		DynamicForm form = form().bindFromRequest();
		String key = form.get("key");
		String transactionData = form.get("transaction");
		
		System.out.println("Transaction data: " + transactionData);
		
		if(!Authentication.sessionAuthOK(key)){
			System.out.println("Auth failed.");
			return forbidden(ReturnCodes.prefix(ReturnCodes.ERROR_WRONG_DATA, "Authentication failed!"));
		}
		
		//handle transaction
		JsonNode post = Json.parse(transactionData);
		Transaction t = Transaction.parseJSON(post);
		System.out.println("About to save t");
		
		t.save();
		
		//handle debts
		t.associatedDebts = Debt.parseDebtorsArray(post.get("debtors"), t);
		for(Debt d: t.associatedDebts) {
			d.to = t.issuer;
			t.totalValue += d.value;
		}
		
		System.out.println("About to update balances");
		
		//Apply algorithms
		AlgoDebts.updateBalanceWithTransaction(t, Session.find.where().eq("keyString", key).findUnique().owner);
		
		t.save();
		
		return ok(ReturnCodes.prefix(ReturnCodes.REQUEST_ACCEPTED, "Transaction saved"));
	}

}
