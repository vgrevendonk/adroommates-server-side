package models;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import play.db.ebean.Model;
import tools.Tools;

@Entity
public class Session extends Model {
	private static final long serialVersionUID = -6111608082703517322L;
	
	public final static Finder<Integer, Session> find = new Finder<Integer, Session>(Integer.class, Session.class);
	
	@Id
	@GeneratedValue
	public int id;
	
	@ManyToOne
	public Member owner;
	
	public String keyString; 
	
	@Temporal(TemporalType.TIME)
	public Date creationDate;

	//Utilities
	public void generateKey(Member owner) {
		keyString = Tools.encodeToSHA1(Integer.toString(owner.id) + Long.toString(Calendar.getInstance().getTimeInMillis()));
	}

}
