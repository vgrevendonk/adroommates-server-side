package models;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.node.ObjectNode;

import play.db.ebean.Model;
import play.libs.Json;

@Entity
public class Transaction extends Model{
	private static final long serialVersionUID = 3187019307817551385L;
	
	public final static Finder<Integer, Transaction> find = new Finder<Integer, Transaction>(Integer.class, Transaction.class);
	
	@Id
	@GeneratedValue
	public int id;
	public double totalValue;
	
	@ManyToOne
	public Member issuer;
	
	@Temporal(TemporalType.TIME)
	public Date issued;
	
	@OneToMany(mappedBy = "relatedTransaction")
	public List<Debt> associatedDebts;
	
	public static List<Transaction> all() {
		return find.all();
	}
	
	public static Transaction findByIssuer(String _issuer) {
		return find.where().eq("issuer", _issuer).findUnique();
	}
	
	public static Transaction findById(int _id) {
		return find.where().eq("id", _id).findUnique();
	}

	//Utilities
	public ObjectNode toJSON() {
		ObjectNode node = Json.newObject();
		node.put("total_value", totalValue);
		node.put("issuer", issuer.id);
		node.put("date", issued.toString());
	
		ObjectNode debtsNode = Json.newObject();
		for(Debt d: associatedDebts) {
			debtsNode.put(Integer.toString(d.from.id), d.value);
		}
		
		node.put("debts", debtsNode);
		
		return node;
	}
	/**
	 * <p>This parses JSON, obviously.</p>
	 * <p><strong>However,</strong> this method does <em>not</em> fill out the debts field or totalValue field.
	 * Simply because Debts need the transaction to exist before they do, and because the transaction needs the debts
	 * to exist before it can know the total value.</p>
	 * @param node
	 * @return
	 */
	public static Transaction parseJSON(JsonNode node){
		Transaction out = new Transaction();
		out.issued = Calendar.getInstance().getTime();
		out.issuer = Member.find.byId(node.get("issuer").asInt());
		out.associatedDebts = new ArrayList<Debt>();
		out.totalValue = 0.0;
		
		return out;
	}
}
