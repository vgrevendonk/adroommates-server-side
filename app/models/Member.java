package models;


import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.codehaus.jackson.node.ObjectNode;

import play.db.ebean.Model;
import play.libs.Json;

@Entity
public class Member extends Model {
	private static final long serialVersionUID = 6971958063642456967L;
	
	public static final Finder<Integer, Member> find = new Finder<Integer, Member>(Integer.class, Member.class); 
	
	@Id
	@GeneratedValue
	public int id;
	
	public String emailAddress; //Used for login
	public String password;
	public String firstName;
	public String lastName;
	public String phoneNumber;
	public double balance;
	
	@OneToMany
	public List<Transaction> transactions;
	
	
	//Utilities
	public String getFullName() {
		return firstName + " " + lastName;
	}
	
	public static Member findByLogin(String _login) {
		return find.where().eq("login", _login).findUnique();
	}
	
	public static List<Member> all() {
		return find.all();
	}
	
	public ObjectNode toJSON() {
		ObjectNode node = Json.newObject();
		node.put("email", emailAddress);
		node.put("firstname", firstName);
		node.put("lastname", lastName);
		node.put("phone", phoneNumber);
		node.put("balance", balance);
		
		return node;
	}

	
	@Override
	public boolean equals(Object o) {
		return ((Member)o).id == id;
	}
}
