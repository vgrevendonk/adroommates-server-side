package models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map.Entry;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.codehaus.jackson.JsonNode;

import play.db.ebean.Model;
import tools.DataErrorException;

@Entity
public class Debt extends Model {
	private static final long serialVersionUID = -2547914243279545705L;
	
	@Id
	@GeneratedValue
	public int id;
	
	public double value;
	
	@ManyToOne
	public Member from;
	
	@ManyToOne
	public Member to;

	@ManyToOne
	public Transaction relatedTransaction;

	public static Debt parseJSON(JsonNode node, Transaction t) throws DataErrorException {
		Debt out = new Debt();
		
		if((out.from = Member.find.byId(node.get("from").asInt())) == null)
			throw new DataErrorException("'from' value in debt is not known as a member ID");
		
		if((out.to = Member.find.byId(node.get("to").asInt())) == null)
			throw new DataErrorException("'from' value in debt is not known as a member ID");
		
		out.value = node.get("value").asDouble();
		
		out.relatedTransaction = t;
		
		return out;
	}
	
	public static ArrayList<Debt> parseDebtorsArray(JsonNode node, Transaction t) {
		ArrayList<Debt> debts = new ArrayList<Debt>();
		
		Iterator<Entry<String, JsonNode>> it = node.getFields();
		
		while(it.hasNext()) {
			Debt out = new Debt();
			Entry<String, JsonNode> n = it.next();
			out.from = Member.find.byId(Integer.parseInt(n.getKey()));
			out.relatedTransaction = t;
			out.value = n.getValue().asDouble();
			debts.add(out);
		}
		
		return debts;
	}
}
