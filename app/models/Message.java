package models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.node.ObjectNode;

import play.db.ebean.Model;
import play.db.ebean.Model.Finder;
import play.libs.Json;

@Entity
public class Message extends Model {

	private static final long serialVersionUID = 7487696091366570974L;
	
	public static final Finder<Integer, Message> find = new Finder<Integer, Message>(Integer.class, Message.class);
	
	
	@Id
	@GeneratedValue
	public int id;
	public String body;
	
	@ManyToOne
	public Member addedBy;
	
	@Temporal(TemporalType.TIME)
	public Date sent;
	
	public ObjectNode toJSON(){
		ObjectNode node = Json.newObject();
		node.put("Body", body);
		node.put("addedBy", addedBy.id);
		node.put("date", sent.toString());
		
		return node;
	}
}
