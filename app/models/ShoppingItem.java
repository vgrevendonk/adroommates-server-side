package models;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.node.ObjectNode;

import play.db.ebean.Model;
import play.db.ebean.Model.Finder;
import play.libs.Json;

@SuppressWarnings("unused")
@Entity
public class ShoppingItem extends Model {
	private static final long serialVersionUID = 3699767163920278256L;
	
	public static final Finder<Integer, ShoppingItem> find = new Finder<Integer, ShoppingItem>(Integer.class, ShoppingItem.class);
	
	@Id
	@GeneratedValue
	public int id;
	public String content;
	
	@Temporal(TemporalType.TIME)
	public Date added;
	
	@ManyToOne
	public Member addedBy;
	
	public static ShoppingItem findById(int _id) {
		return find.where().eq("id", _id).findUnique();
	}
	
	//Utilities
	public ObjectNode toJSON() {
		ObjectNode node = Json.newObject();
		
		node.put("content", content); //TODO parse!!
		node.put("added", added.toString());
		node.put("added_by", addedBy.id);
		
		return node;
	}
	public static ShoppingItem parseJSON(JsonNode node) {
		ShoppingItem out = new ShoppingItem();
		out.id = node.get("id").asInt();
		out.content = node.get("content").asText();
		
		return out;
	}

	@Override
	public boolean equals(Object o) {
		return ((ShoppingItem)o).id == id;
	}
}
