package tools;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * These static functions are self-explanatory.
 * @author Vincent Grevendonk
 *
 */
public class Tools {
	public static String encodeToSHA1(String pass) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA1");
			byte[] encoded;
			md.update(pass.getBytes());
			encoded = md.digest();
			return bytesToHex(encoded);

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private static String bytesToHex(byte[] b) {
		char hexDigit[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'a', 'b', 'c', 'd', 'e', 'f' };
		StringBuffer buf = new StringBuffer();
		for (int j = 0; j < b.length; j++) {
			buf.append(hexDigit[(b[j] >> 4) & 0x0f]);
			buf.append(hexDigit[b[j] & 0x0f]);
		}
		return buf.toString();
	}
}
